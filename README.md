# So you want to...

- Use your ideas and experiences to make CoLab better.

# By...

- Creating a new process
- Discussing or changing an existing process

## Intention

To create an environment in which all aspects of Colab's internal operations can be revised and iterated upon by any motivated collaborator.

## Function

Provide guidelines for creating new processes to test and iterate on.

## Criterion for Success

- Collaborators are consistently using, discussing, and contributing to internal processes.
- Collaborators feel they have a clear way to expressing their creative ideas.

### I want to suggest a new process.

### I want to revise a process.

- Fork the process.
- Make your changes.
- Make a pull request.

### I want to raise a concern about a process.

- Use Github Issues to start a discussion about a particular process.

Examples of good concerns:

- Describe a personal experience.
- Describe a specific reason why you think the process should change.

### Reviewing Pull Requests

- At least 3 CoLab members should thumbs-up the request.
